﻿using System;
using System.Globalization;

namespace _02_DZ
{
    internal static class Foo
    {
        /// <summary>
        ///     Косинус угла <paramref name="degry" /> в градусах.
        /// </summary>
        public static double CosD(this double degry)
        {
            return Math.Cos(Math.PI / 180 * degry);
        }

        /// <summary>
        ///     Синус угла <paramref name="degry" /> в градусах.
        /// </summary>
        public static double SinD(this double degry)
        {
            return Math.Sin(Math.PI / 180 * degry);
        }

        /// <summary>
        ///     Перевод угла <paramref name="degry" /> из радиан в градусы
        /// </summary>
        public static double ToDegry(this double degry)
        {
            return 180 * degry / Math.PI;
        }

        public static string PrintAll(this Star star)
        {
            return "MGT: " + star["MGT"].ToString(CultureInfo.InvariantCulture) + "\n" +
                   "Ra: " + star["RA"].ToString(CultureInfo.InvariantCulture) + "   " +
                   "Dec: " + star["DEC"].ToString(CultureInfo.InvariantCulture) + "\n" +
                   "X: " + star["XYZ", 0].ToString(CultureInfo.InvariantCulture) + "\n" +
                   "Y: " + star["XYZ", 1].ToString(CultureInfo.InvariantCulture) + "\n" +
                   "Z: " + star["XYZ", 2].ToString(CultureInfo.InvariantCulture);
        }
    }
}