﻿using System;
using System.Numerics;

namespace _02_DZ
{
    /// <summary>
    /// Класс Звезда. Содержит в себе информацию о расположении звезды, а так же её интенсивности в каждом пикселе вокруг звезды.
    /// Позволяет передавать в конструктор координаты цента масс звезды на изображении и получить интенсивность пикселей вокруг неё.
    /// </summary>
    internal class Star : IEquatable<Star>
    {
        public bool Equals(Star other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _xyz.Equals(other._xyz) && _ra.Equals(other._ra) && _dec.Equals(other._dec) && _mgt.Equals(other._mgt);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Star) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_xyz, _ra, _dec, _mgt);
        }

        /// <summary>
        /// Конструктор принимает расположение в декартовой системе координат через вектор <paramref name="xYz"/>,
        /// а так же звёздную величину <paramref name="mgt"/>
        /// </summary>
        public Star(Vector3 xYz, double mgt)
        {
            _xyz = xYz;
            this._mgt = mgt;

            _ra = Math.Asin(_xyz.Z).ToDegry();
            _dec = Math.Atan2(_xyz.Y, _xyz.X).ToDegry();
        }

        /// <summary>
        /// Конструктор принимает расположение в декартовой системе координат через соответствующие координаты
        /// [ <paramref name="x"/>;<paramref name="y"/>;<paramref name="z"/>],
        /// а так же звёздную величину <paramref name="mgt"/>
        /// </summary>
        public Star(float x, float y, float z, double mgt)
        {
            _xyz = new Vector3(x, y, z);
            this._mgt = mgt;

            _ra = Math.Asin(_xyz.Z).ToDegry();
            _dec = Math.Atan2(_xyz.Y, _xyz.X).ToDegry();
        }

        /// <summary>
        /// Конструктор принимает расположение в полярной системе координат через соответствующие координаты [<paramref name="ra"/>;<paramref name="dec"/>],
        /// а так же звёздную величину <paramref name="mgt"/>
        /// </summary>
        public Star(double ra, double dec, double mgt)
        {
            this._ra = ra;
            this._dec = dec;
            this._mgt = mgt;

            _xyz = new Vector3(
                Convert.ToSingle(dec.CosD() * ra.CosD()),
                Convert.ToSingle(dec.CosD() * ra.SinD()),
                Convert.ToSingle(dec.SinD()));
        }

        #region координаты звезды в декартовой системе

        private readonly Vector3 _xyz;

        #endregion координаты звезды в декартовой системе

        #region координаты звезды в полярной системе

        private readonly double _ra;
        private readonly double _dec;

        #endregion координаты звезды в полярной системе

        private readonly double _mgt;// звёздная величина

        /// <summary>
        /// Получение координат звезды в полярной системе координат,
        /// а так же звёздной вылечены.
        /// укажите в <paramref name="index"/> "RA" для получение Ra,
        /// укажите в <paramref name="index"/> "DEC, для получения Dec.
        /// укажите в <paramref name="index"/> "MGT, для получения звёздной вылечены.
        /// </summary>
        public double this[string index]
        {
            get
            {
                if (string.Equals(index.ToUpper(), "RA", StringComparison.Ordinal)) return _ra;
                if (string.Equals(index.ToUpper(), "DEC", StringComparison.Ordinal)) return _dec;
                if (string.Equals(index.ToUpper(), "MGT", StringComparison.Ordinal)) return _mgt;
                throw new IndexOutOfRangeException();
            }
        }

        /// <summary>
        ///     Получение координат звезды в декартовой системе
        ///     укажите в <paramref name="sindex" /> "XYZ", для получения координат в формате XYZ,
        ///     параметр <paramref name="iindex" /> принимает 0,1,2 соответствуя X,Y,Z.
        /// </summary>
        public double this[string sindex, int iindex]
        {
            get
            {
                if (string.Equals(sindex.ToUpper(), "XYZ", StringComparison.Ordinal))
                {
                    if (iindex == 0) return Convert.ToDouble(_xyz.X);
                    if (iindex == 1) return Convert.ToDouble(_xyz.Y);
                    if (iindex == 2) return Convert.ToDouble(_xyz.Z);
                }

                throw new IndexOutOfRangeException();
            }
        }

        /// <summary>
        /// Получение интенсивности пикселей с координатами [<paramref name="x"/>; <paramref name="y"/>]
        /// при проекции свечения на изображении.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public double this[int x, int y]
        {
            get
            {
                const double s = 4.6979157650179255;
                double signal = Math.Pow(10, (4 - _mgt / 2.5));
                return signal * ((Erf((_xyz.X - x + 1) / s) - Erf((_xyz.X - x) / s)) *
                    (Erf((_xyz.Y - y + 1) / s) - Erf((_xyz.Y - y) / s)));

                // Функция ошибки Гаусса
                static double Erf(double xx)
                {
                    const double a1 = 0.254829592;
                    const double a2 = -0.284496736;
                    const double a3 = 1.421413741;
                    const double a4 = -1.453152027;
                    const double a5 = 1.061405429;
                    const double p = 0.3275911;

                    int sign = 1;
                    if (xx < 0)
                        sign = -1;
                    xx = Math.Abs(xx);

                    double t = 1.0 / (1.0 + p * xx);
                    double yy = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-xx * xx);

                    return sign * yy;
                }
            }
        }

        public static bool operator !=(Star star1, Star star2)
        {
            if (star2 is { } && star1 is { } && !star1["RA"].Equals(star2["RA"]) && !star1["RA"].Equals(star2["RA"]) &&
                !star1["MGT"].Equals(star2["MGT"]))
                return true;

            return false;
        }

        public static bool operator ==(Star star1, Star star2)
        {
            if (star1 == null) throw new ArgumentNullException(nameof(star1));
            if (star2 == null) throw new ArgumentNullException(nameof(star2));
            if ((star1["RA"].Equals(star2["RA"])) && (star1["RA"].Equals(star2["RA"])) && (star1["MGT"].Equals(star2["MGT"])))
                return true;

            return false;
        }

        public static Star operator +(Star star, double degry) => new Star(star["RA"] + degry, star["DEC"] + degry, star["MGT"]);

        public static Star operator +(double degry, Star star) => star + degry;

        public static Star operator -(Star star, double degry) => star + (-degry);

        public static Star operator -(double degry, Star star) => star - degry;
    }
}