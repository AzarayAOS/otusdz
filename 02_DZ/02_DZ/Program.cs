﻿using System;

namespace _02_DZ
{
    internal static class Program
    {
        private static void Main()
        {
            Star star = new Star(180, -15, 3);
            Console.WriteLine(star.PrintAll());
            Console.WriteLine();

            Star star2 = star + 5;
            Console.WriteLine(star2.PrintAll());
            Console.WriteLine();

            Console.WriteLine("Звёзды " + (Equals(star, star2) ? "равны!" : "не равны!"));

            Star star3 = 10 + star2 + 5;
            Console.WriteLine(star3.PrintAll());
            Console.WriteLine();

            int[,] pic = new int[30, 30];

            //for (int i = 0; i < pic.GetLength(0); i++)
            //{
            //    for (int j = 0; j < pic.GetLength(1); j++)
            //        Console.Write(pic[i, j]);
            //    Console.WriteLine();
            //}
            //Console.WriteLine("\n");

            //Схема интенсивности звезды на каждом пикселе
            star = new Star(15, 15, 0, 3);

            for (int i = 0; i < pic.GetLength(0); i++)

                for (int j = 0; j < pic.GetLength(1); j++)
                {
                    double pix = star[i, j];
                    pic[i, j] = Convert.ToInt32(pix);
                }

            for (int i = 0; i < pic.GetLength(0); i++)
            {
                for (int j = 0; j < pic.GetLength(1); j++)
                    Console.Write($"{pic[i, j],3}");
                Console.WriteLine();
            }
        }
    }
}