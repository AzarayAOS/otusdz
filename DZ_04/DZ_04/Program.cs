﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;

namespace DZ_04
{
    internal class Program
    {
        private static void Main(string[] args)
        {
        }
    }

    public class CatalogStar
    {
        private List<ICatalogStar> _catalog;
    }

    public interface IStar
    {
        int Id { get; }
        double Ra { get; }
        double Dec { get; }
        double Mgt { get; }

        public Vector3 GetElseSystem();
    }

    internal class Star : IStar
    {
        public int Id { get; private set; }

        public double Ra { get; private set; }

        public double Dec { get; private set; }

        public double Mgt { get; private set; }

        public Vector3 GetElseSystem()
        {
            float CosD(double degry) => Convert.ToSingle(Math.Cos(Math.PI / 180 * degry));

            float SinD(double degry) => Convert.ToSingle(Math.Sin(Math.PI / 180 * degry));

            return new Vector3(
                CosD(Dec) * CosD(Ra),
                CosD(Dec) * SinD(Ra),
                SinD(Dec));
        }
    }
}