﻿using System.Collections.Generic;
using System.Linq;

using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users,
            IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public IEnumerable<Account> Accounts { get; }

        public IEnumerable<User> Users { get; }

        public IEnumerable<OperationsHistory> History { get; }

        //1
        /// <summary>
        ///     Получение информации по <paramref name="login" /> и <paramref name="password" />
        /// </summary>
        public Account GetAcc(string login, string password)
        {
            //TODO Сделать проверку, если указанного логина и пароля нет

            var temp = Users.FirstOrDefault(x => x.Login == login && x.Password == password);

            return Accounts.FirstOrDefault(x => temp != null && x.UserId == temp.Id);
        }

        //2
        /// <summary>
        ///     Получение истории пользователя по его <paramref name="ID" />
        /// </summary>
        public List<Account> GetUser(int ID)
        {
            //TODO Сделать проверку, если указанного ID нет
            //var user = Users.Where(x => x.Id == ID);
            return Accounts.Where(x => x.UserId == ID).ToList();
        }

        //2
        /// <summary>
        ///     Получение истории пользователя по его <paramref name="user" />
        /// </summary>
        public List<Account> GetUser(User user)
        {
            return GetUser(user.Id);
        }

        //3
        /// <summary>
        ///     Получение данных о всех счетах заданного пользователя, включая историю по каждому счёту;
        /// </summary>
        public List<OperationsHistory> GetHistories(int id)
        {
            return History.Where(x => x.AccountId == id).ToList();
        }

        //3
        /// <summary>
        ///     Получение данных о всех счетах заданного пользователя, включая историю по каждому счёту;
        /// </summary>
        public List<OperationsHistory> GetHistories(Account account)
        {
            return GetHistories(account.Id);
        }

        //4
        /// <summary>
        ///     Запрос данных о всех операциях пополнения счёта по <paramref name="id" />
        /// </summary>
        public List<OperationsHistory> GetOperationsHistories()
        {
            return History.Where(x => x.OperationType == OperationType.InputCash).ToList();
        }

        /// <summary>
        ///     Возврат всех пользователей, на счете которых больше <paramref name="N" />
        /// </summary>
        public List<User> GetUsersUpN(decimal N)
        {
            var acc = Accounts.Where(x => x.CashAll > N).ToList();
            return acc.Select(t => Users.FirstOrDefault(x => x.Id == t.UserId)).ToList();
        }

        //TODO: Добавить методы получения данных для банкомата
    }
}