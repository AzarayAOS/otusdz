﻿using System;
using System.Linq;

using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов

            #region 1 задание

            {
                System.Console.WriteLine("1");

                var temp = atmManager.GetAcc("star", "444");

                System.Console.WriteLine($"ID: {temp.Id,3};\n" +
                                         $"OpeningDate: {temp.OpeningDate.ToString()}\n" +
                                         $"CashSum: {temp.CashAll:### ### ###}\n" +
                                         $"UserID: {temp.UserId,3}");
            }

            #endregion 1 задание

            System.Console.WriteLine("\n");

            #region 2 задание

            {
                System.Console.WriteLine("2");

                // получить рандомного юзера из списка
                var temp = atmManager.GetUser(
                    atmManager.Users.ToList()
                    [new Random().Next(
                        atmManager.Users.ToList().Count)]);

                System.Console.WriteLine("|--|-------------------|--------|---|");
                System.Console.WriteLine($"|ID|{"OpeningDate",19}|{"CashSum",8}|UID|");
                System.Console.WriteLine("|--|-------------------|--------|---|");
                foreach (var t in temp)
                    System.Console.WriteLine($"|{t.Id,2}" +
                                             $"|{t.OpeningDate.ToString(),19}" +
                                             $"|{t.CashAll,8:### ###}" +
                                             $"|{t.UserId,3}|");
                System.Console.WriteLine("|--|-------------------|--------|---|");
            }

            #endregion 2 задание

            System.Console.WriteLine("\n");

            #region 3 задание

            {
                System.Console.WriteLine("3");

                System.Console.WriteLine("|--|-------------------|--------|---|" +
                                         "---|-------------------|------|--------|");
                System.Console.WriteLine($"|ID|{"OpeningDate",19}|{"CashSum",8}|UID|" +
                                         $"HID|{"OpeningDate",19}|{"OpType",6}|{"CshSumH",8}|");
                System.Console.WriteLine("|--|-------------------|--------|---|" +
                                         "---|-------------------|------|--------|");

                // получить рандомного юзера из списка
                var temp = atmManager.GetUser(
                    atmManager.Users.ToList()
                    [new Random().Next(
                        atmManager.Users.ToList().Count)]);

                foreach (var t in temp)
                {
                    System.Console.WriteLine($"|{t.Id,2}" +
                                             $"|{t.OpeningDate.ToString(),19}" +
                                             $"|{t.CashAll,8:### ###}" +
                                             $"|{t.UserId,3}|" +
                                             "---|-------------------|------|--------|");
                    var histemp = atmManager.GetHistories(t);
                    foreach (var q in histemp)
                        System.Console.WriteLine("|--|-------------------|--------|---|" +
                                                 $"{q.Id,3}" +
                                                 $"|{q.OperationDate.ToString(),19}" +
                                                 $"|{(q.OperationType == OperationType.InputCash ? "Input" : "Output"),6}" +
                                                 $"|{q.CashSum,8:### ###}|");
                }

                System.Console.WriteLine("|--|-------------------|--------|---|" +
                                         "---|-------------------|------|--------|");
            }

            #endregion 3 задание

            System.Console.WriteLine("\n");

            #region 4 задание

            {
                System.Console.WriteLine("4");

                System.Console.WriteLine("|---|-------------------|--------|--------|");
                System.Console.WriteLine($"|HID|{"OpeningDate",19}|{"CshSumH",8}|{"UsLogin",8}|");
                System.Console.WriteLine("|---|-------------------|--------|--------|");
                var temp = atmManager.GetOperationsHistories();

                foreach (var q in temp)
                {
                    var userTemp = atmManager.Users.FirstOrDefault(
                        x => x.Id == atmManager.Accounts.FirstOrDefault(
                            x => x.Id == q.AccountId).UserId);

                    System.Console.WriteLine($"|{q.Id,3}" +
                                             $"|{q.OperationDate.ToString(),19}" +
                                             $"|{q.CashSum,8:### ###}" +
                                             $"|{userTemp.Login,8}|");
                }

                System.Console.WriteLine("|---|-------------------|--------|--------|");
            }

            #endregion 4 задание

            System.Console.WriteLine("\n");

            #region 5 задание

            {
                System.Console.WriteLine("5");

                var temp = atmManager.GetUsersUpN(10000m);

                System.Console.WriteLine(
                    "|--|--------|---------|----------|---------|------------|-------------------|--------|");
                System.Console.WriteLine(
                    $"|ID|{"SurName",8}|{"FirstName",9}|{"MiddleName",10}|{"Phone",9}|{"Pass&Ser#",12}|{"RegistrationDate",19}|{"Login",8}|");
                System.Console.WriteLine(
                    "|--|--------|---------|----------|---------|------------|-------------------|--------|");

                foreach (var t in temp)
                    System.Console.WriteLine(
                        $"|{t.Id,2}" +
                        $"|{t.SurName,8}" +
                        $"|{t.FirstName,9}" +
                        $"|{t.MiddleName,10}" +
                        $"|{string.Format("{0:###-##-##}", t.Phone),9}" +
                        $"|{t.PassportSeriesAndNumber,12}" +
                        $"|{t.RegistrationDate.ToString(),19}" +
                        $"|{t.Login,8}|");

                System.Console.WriteLine(
                    "|--|--------|---------|----------|---------|------------|-------------------|--------|");
            }

            #endregion 5 задание

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        private static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}