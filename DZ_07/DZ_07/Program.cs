﻿using System;
using System.Net;
using System.Text.RegularExpressions;

namespace DZ_07
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            #region Получаем код страницы

            Console.WriteLine("Введите сайт: ");
            var url = Console.ReadLine();

            // проверка на ввод http, иначе WebClienе не выдаст ничего
            const string http = "http";
            if (url.ToLower().IndexOf(http, StringComparison.Ordinal) == -1)
                url = "http://" + url;

            var wClient = new WebClient();

            var htmlcode = wClient.DownloadString(url);

            //Console.WriteLine(htmlcode);

            #endregion Получаем код страницы

            HtmpToUrl(htmlcode);
        }

        /// <summary>
        ///     Выделение адресов из
        ///     <param name="html">кода HTML</param>
        /// </summary>
        private static void HtmpToUrl(string html)
        {
            Match m;
            const string pattern =
                @"href\s*=\s*(?<a>(?:['""]([^\s*]+)['""])|([^\s>]+))";

            try
            {
                m = Regex.Match(html, pattern);

                while (m.Success)
                {
                    Console.WriteLine($"Найдено: {m.Groups[1]} ");
                    m = m.NextMatch();
                }
            }
            catch (RegexMatchTimeoutException)
            {
                Console.WriteLine("RegexMatchTimeoutException");
            }
        }
    }
}